<!DOCTYPE html>
<html>
<head>
	<meta content="text/html charset=UTF-8" http-equiv="content-type">
	
	<?php echo $template->getJS() ?>
	
	<?php echo $template->getCSS() ?>
	
	<title><?php echo $template->getTitle() ?></title>
</head>

<body>
	<header>
		<div class="navbar navbar-beaver navbar-fixed-left">
			<a class="navbar-brand logo" href="/"></a>

			<p>BP<br>
			<?php include RESOURCE_DIR_PATH . '/mod/bp/tpl/index.php'; ?>
			</p>