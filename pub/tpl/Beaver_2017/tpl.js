$(function(){
    
    $(".input-group-btn .dropdown-menu li a").click(function(){

        var selText = $(this).html();
    
        //working version - for single button //
       //$('.btn:first-child').html(selText+'<span class="caret"></span>');  
       
       //working version - for multiple buttons //
       $('#filter').val($(this).attr('data-value'));
       $(this).parents('.input-group-btn').find('.btn-search').html(selText);

   });
    
    $('.input-group.date').datepicker({
    	format: "dd.mm.yyyy",
        language: "de",
        todayHighlight: true,
        daysOfWeekDisabled: "0,6",
        autoclose: true
    });
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    
    $(document).ready(function () {
        $('#owner').selectpicker({
        	liveSearch: true,
        	showSubtext: true
        });
    });
    
    $(document).ready(function () {
        $('#street').selectpicker({
        	liveSearch: true,
        	size: 5
        });
    });
});