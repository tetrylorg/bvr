<?php

/**
 * Check if class exist
 */
if (!class_exists("Geocode"))
{
	class Geocode
	{
		/**
		 * @var	string	$provider
		 */
		protected $provider = 'Google';
		
			/**
			 * @desc	Get map provider
			 * @return	string	$provider
			 */
			public function getProvider()
			{
				return $this->provider;
			}
		
			/**
			 * @desc	Set map provider
			 * @param	string	$provider
			 */
			public function setProvider($provider)
			{
				$this->provider = trim($provider);
			}
		
		/**
		 * @var	string	$zip
		 */
		protected $zip;
		
			/**
			 * @desc	Get zip code
			 * @return	string	$zip
			 */
			public function getZip()
			{
				return $this->zip;
			}
			
			/**
			 * @desc	Set zip code
			 * @param	string	$zip
			 */
			public function setZip($zip)
			{
				$this->zip = urlencode($zip);
			}
			
		/**
		 * @var	string	$location
		 */
		protected $location;
		
			/**
			 * @desc	Get location
			 * @return	string	$location
			 */
			public function getLocation()
			{
				return $this->location;
			}
		
			/**
			 * @desc	Set location
			 * @param	string	$location
			 */
			public function setLocation($location)
			{
				$this->location = urlencode($location);
			}
		
		/**
		 * @var	string	$street
		 */
		protected $street;
		
			/**
			 * @desc	Get street
			 * @return	string	$street
			 */
			public function getStreet()
			{
				return $this->street;
			}
		
			/**
			 * @desc	Set street
			 * @param	string	$street
			 */
			public function setStreet($street)
			{
				$this->street = urlencode(iconv(mb_detect_encoding($street,mb_detect_order(),TRUE),"UTF-8",$street));
			}
		
		/**
		 * @var	string	$hnr
		 */
		protected $hnr;
		
			/**
			 * @desc	Get house number
			 * @return	string	$hnr
			 */
			public function getHnr()
			{
				return $this->hnr;
			}
		
			/**
			 * @desc	Set house number
			 * @param	string	$hnr
			 */
			public function setHnr($hnr)
			{
				$this->hnr = urlencode($hnr);
			}
		
		/**
		 * @var	string	$fallback
		 */
		protected $fallback = TRUE;
		
			/**
			 * @desc	Get fallback value
			 * @return	bool	$fallback
			 */
			public function getFallback()
			{
				return $this->fallback;
			}
		
			/**
			 * @desc	Set fallback | if TRUE use other map provider
			 * @param	string	$fallback
			 */
			public function setFallback($fallback)
			{
				$this->fallback = $fallback;
			}
		
		/**
		 * @desc	Call function based on provider to convert address
		 * @return	string|NULL	$value	if provider is not defined
		 */	
		public function Reverse()
		{			
			($this->getProvider() == 'Google')	?	$value = $this->getGMaps()	:	NULL;
			($this->getProvider() == 'OSM')		?	$value = $this->getOSM()	:	NULL;

			return (!empty($value))	?	$value	:	NULL;
		}
		
		/**
		 * @desc	Request xml from Google
		 * @return	string|NULL	$lnglat
		 */
		protected function getGMaps()
		{
			/**
			 * @var	string	$address		Set address for URL request
			 * @var	string	$request_url	Set URL
			 */
			$address = $this->zip . ',' . $this->location . ',' . $this->street . ',' . $this->hnr;
			$request_url = "https://maps.googleapis.com/maps/api/geocode/xml?address=" . $address . "&sensor=true";
			
			/**
			 * @var	SimpleXMLElement	$xml			Load XML or die
			 * @var	string				$xml_status		XML status | Used to check if XML is valid
			 * @var	bool				$xml_location	TRUE if location is equal to requested XML value | FALSE
			 */
			$xml = simplexml_load_file($request_url) or die("Google URL not loading");
			$xml_status = $xml->status;
			$xml_location = FALSE;
			
			/**
			 * @desc	Check if requested XML is valid | if not and fallback is TRUE, call fallback function
			 */
			if ($xml_status != 'ZERO_RESULTS')
			{
				/**
				 * @desc	Check if requested location is equal to predefined location
				 * @var	bool	$xml_location	TRUE if location is equal to requested XML value
				 */
				if ($xml->result->address_component ["2"]->long_name == $this->location || $xml->result->address_component ["3"]->long_name == $this->location)
				{
					$xml_location = TRUE;
				}
				
				/**
				 * @desc	Check if XML status is OK and XML location is TRUE
				 * @var	string	$lat	Temp var for latitude
				 * @var	string	$lng	Temp var for longitude
				 * @var string	$lnglat	Combined longitude/latitude
				 */
				if ($xml_status == "OK" && $xml_location == TRUE)
				{
					$lat = $xml->result->geometry->location->lat;
					$lng = $xml->result->geometry->location->lng;
					$lnglat = "$lng,$lat";
				}
			}			
			else
			{
				/**
				 * @desc	Call fallback function if fallback is TRUE
				 */
				$lnglat = ($this->fallback == TRUE)	?	$this->getOSM()	:	NULL;
			}
			
			/**
			 * @return	string|NULL	$lnglat	Return longitude/latitude
			 */
			return (!empty($lnglat))	?	$lnglat	:	NULL;
		}
		
		/**
		 * @desc	Request xml from OpenStreetMaps | return longitude and latitude
		 * @return	string|NULL	$lnglat
		 */
		protected function getOSM()
		{
			/**
			 * @var	string	$address		Set address for URL request
			 * @var	string	$request_url	Set URL
			 */
			$address = $this->street .','. $this->hnr .','. $this->zip .','. $this->location;
			$request_url = "https://nominatim.openstreetmap.org/search/" . $address . "?format=xml&limit=1";
			
			/**
			 * @var	SimpleXMLElement	$xml		Load XML or die
			 * @var	integer				$xml_id		XML id | Used to check if XML is valid
			 */
			$xml = simplexml_load_file($request_url) or die("OSM URL not loading");
			$xml_id = $xml->place ["place_id"];
			
			/**
			 * @desc	Check if requested XML is valid | if not and fallback is TRUE, call fallback function
			 * @var	string	$lat	Temp var for latitude
			 * @var	string	$lng	Temp var for longitude
			 * @var string	$lnglat	Combined longitude/latitude
			 */
			if (isset($xml_id))
			{
				$lat = $xml->place ["lat"];
				$lng = $xml->place ["lon"];
				$lnglat = "$lng,$lat";
			}
			else
			{
				/**
				 * @desc	Call fallback function if fallback is TRUE
				 */
				$lnglat = ($this->fallback == TRUE)	?	$this->getGMaps()	:	NULL;
			}
			
			/**
			 * @return	string|NULL	$lnglat	Return longitude/latitude
			 */
			return (!empty($lnglat))	?	$lnglat	:	NULL;
		}
	} //END class
	
	/**
	 * Call the class
	 */
	class_exists("Geocode")	?	$geocode = new Geocode()	:	NULL;
	
} //END if class_exists