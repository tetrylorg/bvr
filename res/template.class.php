<?php
/**
 * Check if class exist
 */
if (!class_exists("Template"))
{	
	/**
	 * @desc	Use for sys internal functions
	 * @var	array	$config	Hold the config array from config.php
	 * @var	array	$this->sysConfig	Hold the sysconfig array from /res/sysconfig.php
	 * @var	object	$debug	Create new class instance
	 */
	class Template
	{
		protected $config;
		protected $sysConfig;
		protected $debug;
		
		/**
		 * @desc	Default Constructor | Load config, sysconfig and debug
		 */
		public function __construct() {
			//include config.php
			include ROOT_DIR_PATH . 'config.php';
			$this->config = $config;
			
			//include sysconfig.php
			include RESOURCE_DIR_PATH . 'sysconfig.php';
			$this->sysConfig = $sysConfig;
			
			//include debug.class.php
			require_once RESOURCE_DIR_PATH . 'debug.class.php';
			//$this->debug = new Debug();
		}
		
		/**
		 * @desc	Return title from config.php
		 * @return string	
		 */
		public function getTitle()
		{
			return $this->config["title"];
		}
		
		/**
		 * @desc	Load CSS files
		 * @var		string	$value
		 * @return	string	$value
		 */
		public function getCSS()
		{
			$value =	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_css"] . '">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_css_theme"] . '">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_datepicker_css"] . '">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_select_css"] . '">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["openlayers_css"] . '" type="text/css">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["openlayers_geocoder_css"] . '">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["lib"]["fontawesome"] . '">';
			$value .=	"\n" . '<link rel="stylesheet" href="' . RESOURCE_PATH . $this->sysConfig["assets"]["css"] . '">';
			
			/**
			 * @desc	Check if custom CSS file is available in template folder
			 * @var	string|NULL	$value
			 */
			$value .=	file_exists(PUBLIC_DIR_PATH . $this->sysConfig["tpl"]["css"])	?	"\n" . '<link rel="stylesheet" href="' . PUBLIC_PATH . $this->sysConfig["tpl"]["css"] . '">'	:	NULL;
			
			return $value;
		}
		
		/**
		 * @desc	Load JS files
		 * @var		string	$value
		 * @return	string	$value
		 */
		public function getJS()
		{
			$value =	"\n" . '<script type="text/javascript" src="' . RESOURCE_PATH . $this->sysConfig["lib"]["jquery"] . '"></script>';
			$value .=	"\n" . '<script type="text/javascript" src="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_js"] . '"></script>';
			$value .=	"\n" . '<script type="text/javascript" src="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_datepicker_js"] . '"></script>';
			$value .=	"\n" . '<script type="text/javascript" src="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_datepicker_locales_js"] . '"></script>';
			$value .=	"\n" . '<script type="text/javascript" src="' . RESOURCE_PATH . $this->sysConfig["lib"]["bootstrap_select_js"] . '"></script>';
			$value .=	"\n" . '<script src="' . RESOURCE_PATH . $this->sysConfig["lib"]["openlayers_js"] . '"></script>';
			$value .=	"\n" . '<script src="' . RESOURCE_PATH . $this->sysConfig["lib"]["openlayers_geocoder_js"] . '"></script>';
			
			/**
			 * Check if custom JS file is available in template folder
			 * @var	string|NULL	$value
			 */
			$value .=	file_exists(PUBLIC_DIR_PATH . $this->sysConfig["tpl"]["js"])	?	"\n" . '<script type="text/javascript" src="' . PUBLIC_PATH . $this->sysConfig["tpl"]["js"] . '"></script>'	:	NULL;
			
			return $value;
		}
					
		public function getUrl($param, $target=NULL)
		{
			if (!empty($param))
			{
				$param = urlencode(trim($param));
			}
				
			if (!empty($target))
			{
				$target = urlencode(trim($target));
				$value = '?'. $this->sysConfig["get_name"] . '=' . $param . '&' . $param . '=' . $target;
			}
			else
			{
				$value = '?'. $this->sysConfig["get_name"] . '=' . $param;
			}
			
			return $value;
		}
		
	} //END class
	
	/**
	 * Call the class
	 */
	class_exists("Template")	?	$template = new Template()	:	NULL;
	
} //END if class_exists