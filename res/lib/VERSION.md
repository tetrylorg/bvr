## BOOTSTRAP ##
* URL: http://getbootstrap.com/
* v3.3.7

## Bootstrap Datepicker ##
* URL: https://github.com/uxsolutions/bootstrap-datepicker
* v1.7.0-dev

## Bootstrap Select ##
* URL: http://silviomoreto.github.io/bootstrap-select
* v1.12.1

## Fontawesome ##
* URL: http://fontawesome.io
* v4.7.0

## FPDF ##
* URL: http://www.fpdf.org/
* v1.81

## Openlayers ##
* URL: https://openlayers.org/
* v3.20.1

## Openlayers Geocoder ##
* URL: https://github.com/jonataswalker/ol3-geocoder
* v2.4.1

## jQuery ##
* URL: http://jquery.org
* v3.1.0

## Parsedown ##
* URL: http://parsedown.org
* v1.6.0