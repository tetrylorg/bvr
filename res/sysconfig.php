<?php
/**
 *	Main configuration file
 */

/**
 * Loading user config
 */
require getcwd() . '/config.php';
	
/**
 * SysConfig Array
 * 
 * @var	array	$SysConfig
 */
$sysConfig = array(
		'name'		=> 'Beaver',	//Program name
		'get_name'	=> 'Beaver',	//$_GET name
		'version'	=> '2.0.0',		//Version.Release.Update
		'readme'	=> 'README.md', //Readme
		'license'	=> '<p><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a></p>',
		'mod_path'	=> 'mod/',		//Path to extensions
		'tbl_max'	=> 9999999999,	//Table max
		
		//Library files
		'lib' => array(
			//jQuery
			'jquery'							=> 'lib/mixed/jquery.min.js',
			
			//Bootstrap
			'bootstrap_css'						=> 'lib/bootstrap/css/bootstrap.min.css',
			'bootstrap_css_theme'				=> 'lib/bootstrap/css/bootstrap-theme.min.css',
			'bootstrap_js'						=> 'lib/bootstrap/js/bootstrap.min.js',
			
			//Bootstrap Datepicker
			'bootstrap_datepicker_css'			=> 'lib/bootstrap_datepicker/css/bootstrap-datepicker.min.css',
			'bootstrap_datepicker_js'			=> 'lib/bootstrap_datepicker/js/bootstrap-datepicker.min.js',
			'bootstrap_datepicker_locales_js' 	=> 'lib/bootstrap_datepicker/locales/bootstrap-datepicker.'.$config["lang_date"].'.min.js',
			
			//Bootstrap Select
			'bootstrap_select_css'				=> 'lib/bootstrap_select/css/bootstrap-select.min.css',
			'bootstrap_select_js'				=> 'lib/bootstrap_select/js/bootstrap-select.min.js',
			
			//OpenLayers
			'openlayers_js'						=> 'lib/openlayers/ol.js',
			'openlayers_css'					=> 'lib/openlayers/ol.css',
			
			//OpenLayers Geocoder
			'openlayers_geocoder_js'			=> 'lib/openlayers_geocoder/ol_geocoder.js',
			'openlayers_geocoder_css'			=> 'lib/openlayers_geocoder/ol_geocoder.min.css',
			
			//Fontawesome
			'fontawesome'						=> 'lib/font_awesome/css/font-awesome.min.css',
			
			//FPDF
			'pdf'								=> 'lib/pdf/fpdf.php',
			
			//Parsedown
			'parsedown'							=> 'lib/mixed/parsedown.php'
		),
			
		//Template files
		'tpl' => array(
			'path'	=> 'tpl/' . $config["tpl_name"] . '/',
			'index'	=> '/index.php/',
			'css'	=> 'tpl/'.$config["tpl_name"].'/style.css', //Custom css
			'js'	=> 'tpl/'.$config["tpl_name"].'/tpl.js' //Custom js
		),
			
		//Assets
		'assets' => array(
			'css'		=> 'assets/css/style.css'
		)			
	);
/**
 * Define $_GET Constant
 */
	defined('GetVar')
		or define('GetVar',@$_GET[$sysConfig["get_name"]]);
	
	defined('GetLogin')
		or define('GetLogin',@$_GET["Login"]);
	
	defined('GetLogout')
		or define('GetLogout',@$_GET["Logout"]);
	
	defined('GetUser')
		or define('GetUser',@$_GET["User"]);
	
	defined('GetProposal')
		or define('GetProposal',@$_GET["Proposal"]);
	
	defined('GetOwner')
		or define('GetOwner',@$_GET["Owner"]);
	
	defined('GetPage')
		or define('GetPage',@$_GET["Page"]);
	
	defined('GetShow')
		or define('GetShow',@$_GET["Show"]);
	
	defined('GetAdd')
		or define('GetAdd',@$_GET["Add"]);
	
	defined('GetEdit')
		or define('GetEdit',@$_GET["Edit"]);
	
	defined('GetDelete')
		or define('GetDelete',@$_GET["Delete"]);
	
	defined('GetClose')
		or define('GetClose',@$_GET["Close"]);
	
	defined('GetPDF')
		or define('GetPDF',@$_GET["PDF"]);
	
	defined('GetHistoric')
		or define('GetHistoric',@$_GET["Historic"]);
	
	defined('GetDebug')
		or define('GetDebug',@$_GET["DEBUG"]);
	
/**
 * Define Path Constant
 */
	$URL = (isset($_SERVER['HTTPS']) ?	'http'	:	'http') .'://' . $_SERVER['HTTP_HOST'] . $config["folder"];
	$PATH = getcwd();
	
	defined('ROOT_PATH')
		or define('ROOT_PATH',$URL);
	
	defined('ADMIN_PATH')
		or define('ADMIN_PATH',$URL.'adm/');
	
	defined('RESOURCE_PATH')
		or define('RESOURCE_PATH',$URL.'res/');
	
	defined('PUBLIC_PATH')
		or define('PUBLIC_PATH',$URL.'pub/');
	
	defined('ROOT_DIR_PATH')
		or define('ROOT_DIR_PATH',$PATH.'/');
	
	defined('ADMIN_DIR_PATH')
		or define('ADMIN_DIR_PATH',$PATH.'/adm/');
	
	defined('RESOURCE_DIR_PATH')
		or define('RESOURCE_DIR_PATH',$PATH.'/res/');
	
	defined('PUBLIC_DIR_PATH')
		or define('PUBLIC_DIR_PATH',$PATH.'/pub/');
	
/**
 * PHP ERROR
 */
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
?>