<?php
/**
 * Check if class exist
 */
if (!class_exists("Main"))
{
	/**
	 * @desc	Load required files
	 * @filesource	sysconfig.php
	 */
	require_once 'sysconfig.php';
	
	/**
	 * @desc	Use for sys internal functions
	 * @var	array	$config	Hold the config array from config.php
	 * @var	array	$sysconfig	Hold the sysconfig array from /res/sysconfig.php
	 * @var	object	$debug	Create new class instance
	 */
	class Main
	{
/** function __construct() */
		protected $config;
		protected $sysConfig;
		protected $debug;
		
		/**
		 * @desc	Default Constructor | Load config, sysconfig and debug
		 */
		public function __construct() {
			//include config.php
			include ROOT_DIR_PATH . 'config.php';
			$this->config = $config;
			
			//include sysconfig.php
			include RESOURCE_DIR_PATH . 'sysconfig.php';
			$this->sysConfig = $sysConfig;
			
			//include debug.class.php
			require_once RESOURCE_DIR_PATH . 'debug.class.php';
			//$this->debug = new Debug();
		}

/** function getFiles() */
		/**
		 * @var	string	$filesDir
		 */
		protected $filesDir;
		
			/**
			 * @desc	Get directory
			 * @return	string	$filesDir
			 */
			public function getFilesDir()
			{
				return $this->filesDir;
			}
			
			/**
			 * @desc	Set directory
			 * @param	string	$filesDir
			 */
			public function setFilesDir($filesDir)
			{
				$this->filesDir = trim($filesDir);
			}
		
		/**
		 * @var	string	$filesFilter
		 */
		protected $filesFilter;
			
			/**
			 * @desc	Get filter
			 * @return	string	$filesFilter
			 */
			public function getFilesFilter()
			{
				return $this->filesFilter;
			}
			
			/**
			 * @desc	Set filter
			 * @param	string	$filesFilter
			 */
			public function setFilesFilter($filesFilter)
			{
				$this->filesFilter = trim($filesFilter);
			}
		
		/**
		 * @desc	Get files with path
		 * @param	array	$result
		 * @uses	object	$varname = new getFiles()
		 * @return	array	$result
		 * 
		 * Used like this:
		 * 
		 *  $main->setFilesDir('PATH_TO_FOLDER');
		 *	$main->setFilesFilter('/.php/');
		 * 	$files = $main->getFiles();
		 * 	foreach($files as $key => $array)
		 *	{
		 *		include $files[$key];
		 *		echo "\n";
		 *	}
		 */
		public function getFiles(&$result = array())
		{
			/**
			 * @var	string	$files
			 */
			$files = scandir($this->getFilesDir());
			
			/**
			 * @desc Output files from directory recursive
			 */
			foreach($files as $key => $value)
			{
				/**
				 * @var	string	$path
				 */
				$path = realpath($this->getFilesDir() . DIRECTORY_SEPARATOR . $value);
				
				/**
				 * @desc	Check if ...
				 */
				if (!is_dir($path))
				{
					if (empty($this->getFilesFilter()) || preg_match($this->getFilesFilter(), $path)) $result[] = $path;
				}
/** recursive -> have to test! Not working correct
				else if ($value != '.' && $value != '..')
				{
					$this->setFilesDir($path);
					$this->getFiles($result);
				}
*/
			}
			
			/**
			 * @return	array	$result	Return file with path
			 */
			return $result;
		}

/** function getParsedown() */
		/**
		 * @var	string	$parsedownDir
		 */
		protected $parsedownDir = ROOT_DIR_PATH;
			
			/**
			 * @desc	Get directory
			 * @return	string	$parsedownDir
			 */
			public function getParsedownDir()
			{
				return $this->parsedownDir;
			}
			
			/**
			 * @desc	Set directory
			 * @param	string	$parsedownDir
			 */
			public function setParsedownDir($parsedownDir)
			{
				$this->parsedownDir= trim($parsedownDir);
			}
			
		/**
		 * @var	string	$parsedownFile
		 */
		protected $parsedownFile;
			
			/**
			 * @desc	Get file
			 * @return	string	$parsedownFile
			 */
			public function getParsedownFile()
			{
				return $this->parsedownFile;
			}
			
			/**
			 * @desc	Set file
			 * @param	string	$parsedownFile
			 */
			public function setParsedownFile($parsedownFile)
			{
				$this->parsedownFile= trim($parsedownFile);
			}
		
		/**
		 * @desc	Generate readable output from .md file
		 * @uses	object	$varname = new Parsedown()
		 * @return	string|NULL
		 * 
		 * Used like this:
		 * 
		 *  $main->setParsedownDir('PATH_TO_FOLDER');
		 *	$main->setParsedownFile('README.md');
		 * 	echo $main->getParsedown();
		 */
		public function getParsedown()
		{
			/**
			 * @desc	Check if $parsedownFile var is not empty
			 */
			if (!empty($this->getParsedownFile()))
			{
				/**
				 * @desc	Load lib file defined in sysconfig array
				 * @filesource	/res/sysconfig.php
				 */
				require_once RESOURCE_DIR_PATH . $this->sysConfig["lib"]["parsedown"];
				
				/**
				 * @var	string	$content
				 */
				$content = file_get_contents($this->getParsedownDir() . '/' . $this->getParsedownFile());
				
				/**
				 * @desc	Create new object
				 * @var object	$parsedown
				 */
				$parsedown = new Parsedown();
				
				/**
				 * @var	string	$content_result
				 */
				$content_result = $parsedown->text($content);
			}
			
			/**
			 * @return	string|null	$result	Return parsedown content
			 */
			return $result = (!empty($content_result))	?	'<div style="display: block; padding: 10px;">' . $content_result . '</div>'	:	NULL;
		}
		
		
/** function getInfo() */
		/**
		 * @var	string	$parsedownFile
		 */
		protected $infoParam;
		
			/**
			 * @desc	Get param
			 * @return	string	$infoParam
			 */
			public function getInfoParam()
			{
				return $this->infoParam;
			}
			
			/**
			 * @desc	Set param
			 * @param	string	$infoParam
			 */
			public function setInfoParam($infoParam)
			{
				$this->infoParam= trim($infoParam);
			}
		
		/**
		 * @desc	Show system info
		 * @param	string		$param
		 * @return	string|NULL	$result
		 * 
		 * Used like this:
		 * 
		 *  $main->setInfoParam('Version');
		 * 	echo $main->getInfo();
		 */
		public function getInfo()
		{
			/**
			 * @desc	Show Version
			 */
			($this->getInfoParam() == 'Version')	?	$value = $this->sysConfig["name"] . ' (' . $this->sysConfig["version"] . ')'	:	NULL;
			
			/**
			 * @desc	Show License
			 */
			($this->getInfoParam() == 'License')	?	$value = $this->sysConfig["license"]	:	NULL;
			
			/**
			 * @desc	Show markdown file
			 * @uses	object	$this->getParsedown()
			 */
			if ($this->getInfoParam() == 'Readme')
			{
				$this->setParsedownFile($this->sysConfig["readme"]);
				$value = $this->getParsedown();
			}
			
			return $result = (!empty($value))	?	$value	:	NULL;
		}
				
		public function getTemplate()
		{
			/**
			 * Call the class
			 */
			class_exists("Template")	?	$template = new Template()	:	NULL;
			
			$this->setFilesDir(PUBLIC_DIR_PATH . $this->sysConfig["tpl"]["path"]);
			$this->setFilesFilter($this->sysConfig["tpl"]["index"]);
			
			$files = $this->getFiles();
			foreach($files as $key => $array)
			{
				include $files[$key];
				echo "\n";
			}
		}
		
	} //END class
	
	/**
	 * Call the class
	 */
	class_exists("Main")	?	$main = new Main()	:	NULL;

} //END if class_exists