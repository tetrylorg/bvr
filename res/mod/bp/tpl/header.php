<ul class="nav navbar-nav">
	<li <?php echo (GetVar == 'Proposals') ? 'class="active"' : NULL ?>><a href="<?php echo $template->getUrl('Proposals') ?>">01</a></li>
	<li <?php echo (GetVar == 'Owners') ? 'class="active"' : NULL ?>><a href="<?php echo $template->getUrl('Owners') ?>">02</a></li>
				
	<li class="dropdown <?php echo (GetHistoric == TRUE) ? 'active' : NULL ?>">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">03<span class="caret"></span></a>
		<ul class="dropdown-menu">
		   	<li <?php echo (GetVar == 'Proposals' && GetHistoric == TRUE) ? 'class="active"' : NULL ?>><a href="<?php echo $template->getUrl('ProposalsHistoric') ?>">03.1</a></li>
		   	<li <?php echo (GetVar == 'Owners' && GetHistoric == TRUE) ? 'class="active"' : NULL ?>><a href="<?php echo $template->getUrl('OwnersHistoric','1') ?>">03.2</a></li>
		</ul>
	</li>
</ul>