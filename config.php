<?php
/**
 * You can change everything if needed
 */

$config = array(

	/**
	 * Set page title, template, working folder and default language
	 */
	'title' 				=> 'Beaver',		//Browser window title
	'tpl_name' 				=> 'Beaver_2017',	//Template name
	'folder' 				=> '/beaver/',		//Working folder
	'lang' 					=> 'de-DE',			//Set page language
	'lang_date'				=> 'de',			//Set date language (used for lib datepicker locales)
	'table_limit' 			=> 30,				//Set max. data rows per page (used for pagination)
	'search_table_limit'	=> 100,				//Set max. data rows for search results
		
	/**
	 * SQL Connection Data
	 */
	'db_sys'		=> 'MYSQL',		//Set which database engine will be used | e.g. 'MYSQL' or 'MSSQL'
	'db_host' 		=> '127.0.0.1',	//IP of database host
	'db_name' 		=> 'beaver2',	//Database name
	'db_user' 		=> 'beaver',	//Database user
	'db_pass' 		=> 'b34v3r',	//Database password
	'db_prefix' 	=> 'beaver_',	//Set database prefix e.g. 'bvr_'
	'db_prefix_h'	=> 'historic_',	//Set database prefix for migrated data e.g. 'historic_' (define queries in res/inc/query.class.php if needed!)
		
	/**
	 * Map marker
	 */
	'bvr_marker_np'	=> 'res/assets/img/map-marker-icon-gray.png',	//Set Marker for new proposals
	'bvr_marker_ep'	=> 'res/assets/img/map-marker-icon-blue.png',	//Set Marker for edited proposals
	'bvr_marker_cp'	=> 'res/assets/img/map-marker-icon-green.png',	//Set Marker for closed proposals
	'bvr_marker'	=> 'res/assets/img/map-marker-icon-gear.png',	//Set Marker for sidebar map
	
	/**
	 * Beaver CSS class
	 *
	 * @description	CSS class for styling (e.g. single 'label-default' or multiple 'label-default custom_tpl_css' class)
	 */
		
	/**
	 * Proposal Status (label)
	 */
	'bvr_stat_n'	=> 'label-default',	//Label for new proposals
	'bvr_stat_e'	=> 'label-info',	//Label for edited proposals
	'bvr_stat_c'	=> 'label-success',	//Label for closed proposals
	
	/**
	 * Proposal Status
	 */
	'bvr_prg_n'		=> 'progress-bar-default',	//Progress bar color for new proposals
	'bvr_prg_e'		=> 'progress-bar-info',		//Progress bar color for edited proposals
	'bvr_prg_c'		=> 'progress-bar-success',	//Progress bar color for deleted proposals
	
	/**
	 * Edit Buttons
	 */
	'bvr_btn_a'		=> 'btn-success',	//Add data button color
	'bvr_btn_e'		=> 'btn-default',	//Edit data button color
	'bvr_btn_d'		=> 'btn-danger',	//Delete data button color
	'bvr_btn_c'		=> 'btn-success',	//Close proposal button color
	'bvr_btn_m'		=> 'btn-info',		//Button color for additional info in modal
	
	/**
	 * Save,Edit,Delete Status
	 */
	'bvr_alert_a'	=> 'alert-success',	//Status color for new added data
	'bvr_alert_al'	=> 'label-success',	//Status label color for new added data
	'bvr_alert_e'	=> 'alert-warning',	//Status color for edited data
	'bvr_alert_el'	=> 'label-warning',	//Status label color for edited data
	'bvr_alert_d'	=> 'alert-danger',	//Status color for deleted data
	'bvr_alert_dl'	=> 'label-danger'	//Status label color for deleted data
);
?>