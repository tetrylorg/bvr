<?php
/**
 * @name		Beaver
 * @category	OSS
 * @author		Sebastian Ruwe
 * @copyright	2017 - Sebastian Ruwe
 * @version		$ID: 2.0.0
 * @link		http://oss.ruwe-digital.de
 */

	/**
	 * Loading required main file
	 */
	require 'res/main.php';
?>